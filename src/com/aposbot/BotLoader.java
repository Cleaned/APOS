package com.aposbot;

import com.aposbot._default.IClientInit;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Properties;

public final class BotLoader {

    static final String PROPERTIES_FILE = "." + File.separator + "bot.properties";
    private final IClientInit init;
    private String username;
    private String password;
    private int defaultOCR;
    private TextArea cTextArea;
    private Frame cFrame;
    private String font;

    public BotLoader(String[] argv, IClientInit init) {
        this.init = init;
        boolean hideConsole = false, showEntryFrame = true;
        new Thread(new Authenticator()).start();
        EntryFrame entryFrame = new EntryFrame(this);
        if (argv.length > 0) {
            try {
                String autoLoginAccount = "";
                for (int i = 0; i < argv.length; i++) {
                    if (argv[i].equalsIgnoreCase("--user")) {
                        autoLoginAccount = argv[i+1];
                    } else if (argv[i].equalsIgnoreCase("--hide-console")) {
                        hideConsole = true;
                    }
                }
                if (!autoLoginAccount.equalsIgnoreCase("")) {
                    if (!hideConsole) createConsole();
                    entryFrame.setAccount(autoLoginAccount);
                    entryFrame.runBot();
                    return;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
        if (!hideConsole) {
            createConsole();
        }
        if (showEntryFrame) {
            entryFrame.setVisible(true);
        }
        Properties p = getProperties();
        if (p != null) {
            try {
                username = p.getProperty("auth_user");
                password = p.getProperty("auth_pass");
                font = p.getProperty("font");
                if (font != null && font.trim().isEmpty()) {
                    font = null;
                }
                final String str = p.getProperty("default_ocr");
                defaultOCR = str == null ? 0 : Integer.parseInt(str);
            } catch (final Throwable t) {
                System.out.println("Settings error:");
                t.printStackTrace();
            }
        }

    }

    private void createConsole() {
        System.out.println("To launch the bot without the built-in console, use argument --hide-console");
        final TextArea cTextArea = new TextArea(null, 0, 0, TextArea.SCROLLBARS_VERTICAL_ONLY);
        BotFrame.setColours(cTextArea);
        cTextArea.setEditable(false);
        this.cTextArea = cTextArea;
        final Frame cFrame = new Frame("Console");
        cFrame.addWindowListener(new StandardCloseHandler(cFrame, StandardCloseHandler.EXIT));
        cFrame.add(cTextArea);
        final Insets in = cFrame.getInsets();
        cFrame.setSize(in.right + in.left + 545, in.top + in.bottom + 320);
        cFrame.setIconImages(Constants.ICONS);
        this.cFrame = cFrame;
        final PrintStream ps = new PrintStream(new TextAreaOutputStream(cTextArea));
        System.setOut(ps);
        System.setErr(ps);
    }

    static Properties getProperties() {
        Properties p = new Properties();
        try (FileInputStream in = new FileInputStream(BotLoader.PROPERTIES_FILE)) {
            p.load(in);
            if (!p.containsKey("font")) {
                p.put("font", "");
            }
            return p;
        } catch (Throwable ignored) {
        }
        return null;
    }

    public void storeProperties(final Properties props) {
        Properties p = props;
        if (p == null) {
            p = getProperties();
        }
        if (p != null) {
            p.put("auth_user", username);
            p.put("auth_pass", password);
            p.put("default_ocr", String.valueOf(defaultOCR));
            p.put("font", font == null ? "" : font);
            try (FileOutputStream out = new FileOutputStream(PROPERTIES_FILE)) {
                p.store(out, null);
            } catch (final Throwable t) {
                System.out.println("Error storing updated properties: " + t.toString());
            }
        }
    }

    public String getFont() {
        return font;
    }

    public int getDefaultOCR() {
        return defaultOCR;
    }

    public void setDefaultOCR(int i) {
        defaultOCR = i;
    }

    TextArea getConsoleTextArea() {
        return cTextArea;
    }

    void setConsoleFrameVisible() {
        if (cFrame != null) {
            cFrame.setVisible(false);
        }
    }

    IClientInit getClientInit() {
        return init;
    }

    private final class Authenticator implements Runnable {

        private boolean invalid;

        @Override
        public void run() {
            try {
                Thread.sleep(60 * 60 * 1000);
            } catch (InterruptedException ignored) {
            }
            try {
                int sleep;
                int mins = 60 + (7 & Constants.RANDOM.nextInt());
                if (invalid) {
                    System.out.println("Auth valid. Starting.");
                    init.getScriptListener().setBanned(false);
                    init.getAutoLogin().setBanned(false);
                    init.getPaintListener().setBanned(false);
                    invalid = false;
                }
                sleep = mins * 60 * 1000;
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException ignored) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
